const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .vue()
    .sass('resources/sass/app.scss', 'public/css');
mix.styles([
    
    // slick
    'public/css/slick.css',
    'public/css/slick-theme.css',

    // nouislider 
    'public/css/nouislider.min.css',

    // Font Awesome Icon 
    'public/css/font-awesome.min.css',

    //Custom stlylesheet
    'public/css/style2.css',
     'public/css/rewamp.css',

    //  Postcoder Css
    'public/css/postcoder-autocomplete.css'
    ], 'public/css/all.css');
