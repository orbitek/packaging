<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('products')->delete();
        
        \DB::table('products')->insert(array (
            0 => 
            array (
                'id' => 1,
            'name' => 'Chicken Wings',
                'image_name' => '1.jpg',
                'description' => '<div class="ng-scope">
                <p>1 KG (10-12 pieces)<br />Grade A Chicken</p>
                </div>',
                'price' => 2.49,
                'discount' => 2.49,
                'tag' => 'New',
                'category_id' => 1,
                'created_at' => '2021-02-09',
                'updated_at' => '2021-02-09',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Chicken Drumsticks',
                'image_name' => '1.jpg',
                'description' => '<div class="ng-scope">
                <p>1 KG (10-12 pieces)<br />Grade A Chicken</p>
                </div>',
                'price' => 2.99,
                'discount' => 2.99,
                'tag' => 'New',
                'category_id' => 1,
                'created_at' => '2021-02-09',
                'updated_at' => '2021-02-09',
            ),
            2 =>  
            array (
                'id' => 3,
                'name' => 'Marinated Meatballs',
                'image_name' => '1.jpg',
                'description' => '<div class="ng-scope">
                500Grm
                </div>',
                'price' => 5.49,
                'discount' => 5.49,
                'tag' => 'New',
                'category_id' => 2,
                'created_at' => '2021-02-09',
                'updated_at' => '2021-02-09',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Mix Diced Lamb With Bone',
                'image_name' => '1.jpg',
                'description' => '<div class="ng-scope">
                    Cut from lamb shoulder and lamb leg- a perfect mix of boneless and on the bone meat.
                </div>',
                'price' => 11.99,
                'discount' => 11.99,
                'tag' => 'Hot',
                'category_id' => 2,
                'created_at' => '2021-02-09',
                'updated_at' => '2021-02-10',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Mutton Shoulder',
                'image_name' => '1.jpg',
                'description' => '<ul class="i8Z77e">
                Shoulder with bones
                </ul>',
                'price' => 8.99,
                'discount' => 8.99,
                'tag' => 'Hot',
                'category_id' => 3,
                'created_at' => '2021-02-09',
                'updated_at' => '2021-02-12',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Peri Peri Whole Chicken',
                'image_name' => '1.jpg',
                'description' => '<p>Marinated in peri peri</p>',
                'price' => 4.49,
                'discount' => 4.49,
                'tag' => 'Hot',
                'category_id' => 4,
                'created_at' => '2021-02-09',
                'updated_at' => '2021-02-09',
            ),
            
        ));
        
        
    }
}