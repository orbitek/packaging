<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Role;
use App\Admin;

class AdminsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        
        $admin = Role::where('name', 'admin')->first();
        
        $user = Admin::create([
                'username' => 'admin',
                'name' => 'Usman',
                'password' => '12345',
                'address' => '184 Chorltan',
                'postcode' => 'M21',
                'city' => 'Manchester',
                'phone' => '12345678',
                'status' => 1,
                'created_at' => '2021-08-28',
                'updated_at' => '2021-08-28',

        ]);
        $user->roles()->attach($admin);
        
    }
}