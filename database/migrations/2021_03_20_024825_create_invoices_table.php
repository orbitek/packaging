<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('price');
            $table->integer('shipping_price');
            $table->integer('discount')->default(0);
            $table->integer('total_price');
            $table->string('postcode');
            $table->string('Place');
            $table->string('city');
            $table->integer('paid')->default(0);
            $table->integer('shiping_id');
            $table->string('shipping_time'); 
            $table->string('status')->default('Placed');
            $table->string('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
