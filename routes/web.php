<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/{vue_capture?}', function () {
    return view('welcome');
})->where('vue_capture', '[\/\w\.-]*');

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Dashboard
//login
Route::get('admin', 'loginController@adminIndex')->name('admin.login');
Route::post('admin', 'loginController@adminPosted');
//Mobile Routs
Route::post('/mobile/adminLogin', 'MobileApi\admin\UserController@adminLoginMobile');
Route::get('/mobile/dashboard/{page}', 'MobileApi\admin\DashboardController@dashboard');
Route::POST('/mobile/invoice_update', 'MobileApi\admin\DashboardController@invoiceUpdate')->name('mobileInvoiceUpdate');
Route::POST('/mobile/showInvoice', 'MobileApi\admin\DashboardController@showInvoice')->name('mobileShowInvoice');

Route::group(['middleware' => 'admin'], function(){

 
    Route::get("/admin_panel", 'admin_panel\dashboardController@index')->name('admin.dashboard');
    Route::get("/newOrders", 'admin_panel\dashboardController@newOrders')->name('admin.dashboard.newOrders');
    Route::get("/processingOrders", 'admin_panel\dashboardController@processingOrders')->name('admin.dashboard.processingOrders');
    Route::get("/dispatchedOrders", 'admin_panel\dashboardController@dispatchedOrders')->name('admin.dashboard.dispatchedOrders');
    Route::get("/deliverdOrders", 'admin_panel\dashboardController@deliverdOrders')->name('admin.dashboard.deliverdOrders');


    Route::get('admin/logout', 'loginController@adminLogout')->name('admin.logout');
    //categories
    Route::get('/admin_panel/categories', 'admin_panel\categoriesController@index')->name('admin.categories');
    Route::post('/admin_panel/categories', 'admin_panel\categoriesController@posted');

    Route::get('/admin_panel/categories/edit/{id}', 'admin_panel\categoriesController@edit')->name('admin.categories.edit');
    Route::post('/admin_panel/categories/edit/{id}', 'admin_panel\categoriesController@update');

    Route::get('/admin_panel/categories/delete/{id}', 'admin_panel\categoriesController@delete')->name('admin.categories.delete');
    Route::post('/admin_panel/categories/delete/{id}', 'admin_panel\categoriesController@destroy');

    //Questions
    Route::get('/admin_panel/question', 'admin_panel\QuestionController@index')->name('admin.question');
    Route::post('/admin_panel/question/create', 'admin_panel\QuestionController@store')->name('admin.question.store');

    Route::get('/admin_panel/question/edit/{id}', 'admin_panel\QuestionController@edit')->name('admin.question.edit');
    Route::post('/admin_panel/question/update/{id}', 'admin_panel\QuestionController@update')->name('admin.question.update');

    Route::get('/admin_panel/question/delete/{id}', 'admin_panel\QuestionController@delete')->name('admin.question.delete');
    Route::post('/admin_panel/question/delete/{id}', 'admin_panel\QuestionController@destroy');

    //products
    Route::get('/admin_panel/products', 'admin_panel\productsController@index')->name('admin.products');

    Route::get('/admin_panel/products/create', 'admin_panel\productsController@create')->name('admin.products.create');
    
    Route::post('/admin_panel/products/create', 'admin_panel\productsController@store');

    Route::get('/admin_panel/products/edit/{id}', 'admin_panel\productsController@edit')->name('admin.products.edit');
    Route::post('/admin_panel/products/edit/{id}', 'admin_panel\productsController@update');

    Route::get('/admin_panel/products/delete/{id}', 'admin_panel\productsController@delete')->name('admin.products.delete');
    Route::post('/admin_panel/products/delete/{id}', 'admin_panel\productsController@destroy');
    Route::get('/admin_panel/products/inStock/{id}/{status}', 'admin_panel\productsController@inStock')->name('admin.inStock');
    Route::get('/admin_panel/products/variation/{id}', 'admin_panel\productsController@variation')->name('admin.variation');
    Route::POST('/admin_panel/products/addVariation', 'admin_panel\productsController@addVariation')->name('admin.addVariation');
     Route::POST('/admin_panel/products/updateVariation', 'admin_panel\productsController@updateVariation')->name('admin.updateVariation');
    Route::get('/admin_panel/products/variationEdit/{id}', 'admin_panel\productsController@variationEdit')->name('admin.variation.edit');
    Route::get('/admin_panel/products/variationDelete/{id}', 'admin_panel\productsController@variationDelete')->name('admin.variation.delete');


    //order management 
    Route::get('/admin_panel/management', 'admin_panel\managementController@manage')->name('admin.orderManagement');
    Route::post('/admin_panel/management', 'admin_panel\managementController@update')->name('admin.orderUpdate');

    //User Managment
    Route::get('/admin_panel/adminManagement', 'admin_panel\AdminController@index')->name('admin.adminManagement');
    Route::get('/admin_panel/adminCreate', 'admin_panel\AdminController@create')->name('admin.adminCreate');
    Route::get('/admin_panel/adminEdit/{id}', 'admin_panel\AdminController@edit')->name('admin.adminEdit');
    Route::Post('/admin_panel/adminStore', 'admin_panel\AdminController@store')->name('admin.adminStore');
    Route::Post('/admin_panel/adminUpdate/{id}', 'admin_panel\AdminController@update')->name('admin.adminUpdate');

    //PostCode Managment
    Route::get('PostcodeManagement', 'PostcodeController@index')->name('Postcode.PostcodeManagement');
    Route::get('PostcodeCreate', 'PostcodeController@create')->name('Postcode.PostcodeCreate');
    Route::get('PostcodeEdit/{id}', 'PostcodeController@edit')->name('Postcode.PostcodeEdit');
    Route::Post('PostcodeStore', 'PostcodeController@store')->name('Postcode.PostcodeStore');
    Route::Post('PostcodeUpdate/{id}', 'PostcodeController@update')->name('Postcode.postcodeUpdate');

    //PromoCode
    Route::get('promoManagement', 'PromoController@index')->name('promo.promoManagement');
    Route::Post('promoManagement', 'PromoController@store')->name('promo.store');


});
//Invoice
Route::get('/invoice/{id}', 'InvoiceController@show')->name('invoice.view');
Route::get('/invoice', 'InvoiceController@index')->name('invoice.index');
Route::get('/dashboard_ajax/{any}', 'InvoiceController@dashboard_ajax')->name('invoice.dashboard_ajax');
Route::get('/invoice_update/{id}/{status}', 'InvoiceController@invoice_update')->name('invoice.invoice_update');
Route::get('/invoice_print/{id}', 'InvoiceController@print')->name('invoice.print');
Route::get('/showInvoice/{id}', 'InvoiceController@showInvoice')->name('showInvoice.view');


//Email
Route::get('/mail/{id}', 'InvoiceController@mail')->name('mail');
Route::get('/confermationEmail/{id}', 'InvoiceController@confermationEmail')->name('confermationEmail');
Route::get('/forgotPassworCodeSent', 'signupController@forgotPassworCodeSent')->name('forgotPassworCodeSent');
Route::get('/dispatchedEmail', 'InvoiceController@dispatchedEmail')->name('dispatchedEmail');
Route::get('/forgetPassword', 'signupController@forgetPassword')->name('forgetPassword');
Route::POST('/forgetPasswordLinkSend', 'signupController@forgetPasswordLinkSend')->name('forgetPasswordLinkSend');
Route::Get('/resetPassword/{id}/{email}/{fid}', 'signupController@resetPassword')->name('resetPassword');
Route::Post('/resetPasswordSave', 'signupController@resetPasswordSave')->name('resetPasswordSave');



Route::get('/testmail', 'InvoiceController@testEmail')->name('testmail');



Route::get('/product/{id}', 'admin_panel\productsController@view')->name('product.view');
Route::get('/login', 'loginController@userIndex')->name('user.login');
Route::post('/login', 'loginController@userPosted');

//signup
Route::get('/signup', 'signupController@userIndex')->name('user.signup');
Route::post('/signup', 'signupController@userPosted')->name('userSave');
Route::post('/check_email', 'signupController@emailCheck')->name('user.signup.check_email');
Route::get('/editProfile', 'signupController@editProfile')->name('editProfile');
Route::POST('/updateProfile', 'signupController@updateProfile')->name('updateProfile');

//SMS
Route::get('/signup/sms_verify', 'signupController@sms_verify')->name('sms_verify');
Route::get('/signup/sms_verify_code', 'signupController@sms_verify_code')->name('sms_verify_code');
Route::get('/orderConfirmSMS', 'InvoiceController@orderConfirmSMS')->name('orderConfirmSMS');
Route::get('/orderDispatchedSMS', 'InvoiceController@orderDispatchedSMS')->name('orderDispatchedSMS');


//user
Route::get('/', 'user\userController@index')->name('user.home');

Route::get('/search', 'user\userController@search')->name('user.search');
Route::get('/search?c={id}', 'user\userController@view')->name('user.search.cat');



Route::get('/view/{id}', 'user\userController@view')->name('user.view');
Route::post('/view/{id}', 'user\userController@post')->name('user.postcart');

Route::get('/cart', 'user\userController@cart')->name('user.cart');
Route::post('/cart', 'user\userController@confirm');

Route::post('/edit_cart', 'user\userController@editCart')->name('user.editCart');
Route::post('/delete_item_from_cart', 'user\userController@deleteCartItem')->name('user.deleteCartItem');


Route::get('/logout', 'loginController@userLogout')->name('user.logout');

Route::group(['middleware' => 'user'], function(){
Route::get('/history', 'user\userController@history')->name('user.history');
});

//Checkout
Route::get('/checkout/{cost}', 'InvoiceController@Checkout')->name('checkout');
Route::POST('/Checkout_response', 'InvoiceController@Checkout_response')->name('Checkout_response');

//aboutUs
Route::get('/aboutUs', 'user\userController@aboutUs')->name('aboutUs');
Route::get('/deliveryProcess', 'user\userController@deliveryProcess')->name('deliveryProcess');
Route::get('/privacyPolicy', 'user\userController@privacyPolicy')->name('privacyPolicy');
Route::get('/refundPolicy', 'user\userController@refundPolicy')->name('refundPolicy');
Route::get('/termsConditions', 'user\userController@termsConditions')->name('termsConditions');

// Promo
Route::Post('/promoCheck', 'PromoController@check')->name('promo.check');

//Subscribe
Route::Post('/subscibeNow', 'user\userController@subscribeNow')->name('subscribeNow');

//Maps
Route::get('/maps', 'PostcodeController@maps');

//SiteMap
Route::get('/sitemap', 'user\userController@siteMap');

//favourite
Route::get('/add_favourite/{id}', 'user\userController@add_favourite')->name('user.add_favourite');

Route::get('/createFolder', 'admin_panel\productsController@createFolder');


//test
Route::get("/test", function(){
   return View::make("admin_panel/orders/printInvoice");
});
Route::get("/test_pusher", function(){
    
   return date('Y-m-d H:i:s');
});