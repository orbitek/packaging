import Vue from "vue"
import Router from "vue-router"

Vue.use(Router)

import Home from './components/pages/Shop/Home'
import Dashboard from './components/pages/Admin/Dashboard'

const routes = [
    {
        path: '/home',
        component: Home
    },
    {
        path: '/dashboard',
        component: Dashboard
    }
]

export default new Router({
    mode: 'history',
    routes
})