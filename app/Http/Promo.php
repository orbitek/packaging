<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
     protected $fillable = [
        'type_id',
        'title',
        'value',
        'min_price',
        'limit',
        'description',
        'start_date',
        'end_date',
        'status',
    ];

}
