<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerPromo extends Model
{
     protected $fillable = [
        'invoice_id',
		'customer_id',
        'promo_id',
    ];
}
