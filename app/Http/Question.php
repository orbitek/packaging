<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'question'
    ];
     public function product()
    {
        return $this->belongsToMany('App\Product');
    }
      public function option()
    {
    	return $this->belongsToMany('App\Option','question_id','id');
    }
}
