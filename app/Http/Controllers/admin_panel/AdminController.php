<?php

namespace App\Http\Controllers\admin_panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryVerifyRequest;
use App\Http\Requests\CategoryEditVerifyRequest;
use App\Admin;
use App\Role;
use Auth;
use Session;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $admin = Session::get('admin');
       
       $admins = Admin::all();

        return view('admin_panel.admins.index')
            ->with('admins', $admins);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $data['role'] = Role::all();
        return view('admin_panel.admins.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $user = Admin::create([
            'name' => $request['name'],
            'username' => $request['username'],
            'address' => $request['address'],
            'postcode' => $request['postcode'],
            'city'  => $request['city'],
            'password' => $request['phone'],
            'phone'=>$request['phone'],
            'status'=>1,
        ]);
         $role = Role::where('id',$request['role'])->first();
         $user->roles()->attach($role);
        //$this->userRoleUpdate($request,$user->id);
        return redirect('/admin_panel/adminManagement')->with('flash_success','User added successfully ');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $res = Product::find($id);
        $res1 = Product::all();
        $cat=Category::find($res->category_id);
        $colorList = explode(',',$res->colors);
        $cat = Category::all();
        return view('store.product')
            ->with('product', $res)
            ->with('products', $res1)
            ->with('cat', $cat)
            ->with('colors',$colorList);  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['user'] = Admin::find($id);
        // dd(User::find($id));
        $data['role'] = Role::all();
        $data['user_role']='';
        
        foreach ($data['role'] as $role) {
            $arr = (array)$role->name;
            if ( $data['user']->hasRole($arr) ) {
                $data['user_role'] = $role->id;     
            }            
        }
        //dd($data);
        return view('admin_panel.admins.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
//dd($request);
        $user = Admin::find($id);
        $user->update($request->all());
        $user->roles()->sync($request->role);
        return redirect()->route('admin.adminManagement')->with('success',"User Updated Succesfully");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       

    }
}
