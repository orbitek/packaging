<?php

namespace App\Http\Controllers\admin_panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\sale;
use App\Invoice;
class dashboardController extends Controller
{
    public function index(){


    	if (request()->ajax()) {
    		return datatables()->of(Invoice::latest()->get())->toJson();;
    	}
      
        return view('admin_panel.dashboard.index');
        
    }

    public function newOrders(){
        
        return view('admin_panel.dashboard.newOrders');
       
    }
    public function processingOrders(){
        
        return view('admin_panel.dashboard.processingOrders');
       
    }
    public function dispatchedOrders(){
        
        return view('admin_panel.dashboard.dispatchedOrders');
       
    }
    public function deliverdOrders(){
        
        return view('admin_panel.dashboard.deliverdOrders');
       
    }
    
}
