<?php
namespace App\Http\Controllers\user;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\orderRequest;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\Category;
use App\sale;
use App\User;
use App\Address;
use Session;
use App\Shipping;
use App\Invoice;
use App\InvoiceDetail;
use App\InvoiceQuestion;
use App\Events\NewOrder;
use App\Http\Controllers\InvoiceController;
use App\Variation;
use App\Promo;
use App\CustomerPromo;
use App\Subscriber;
use App\Favourite;


class userController extends Controller
{
    public function index()
    {
        $title = 'Home';
        $fav = "";
        if (session::has('user')) {
            $fav = Favourite::select('product_id')->where('user_id', session('user')->id)->get();
        }

        $res = Product::paginate(6);
        $cat = Category::all();

        return view('store.index')
        ->with('fav', $fav)
        ->with('products', $res)
        ->with('title', $title)
        ->with("cat", $cat)
        ->with('index', 1);
    }
    public function view($id)
    {

        $res = Product::find($id);
        $res1 = Product::all();
        $cat=Category::find($res->category_id);
        $colorList = explode(',',$res->colors);
        $cat = Category::all();
        $var = Variation::where('product_id',$id)->get();
        $title = 'product';
       // dd($var);
        return view('store.product')
        ->with('product', $res)
        ->with('products', $res1)
        ->with('title', $title)
        ->with('cat', $cat)
        ->with('var', $var)
        ->with('colors',$colorList);   
    }

    public function search(Request $r){
        $category ;
        $title = 'Products';
        $name ;
        $fav = "";
        if($r->query("c")){
            $category = $r->query("c");
        }
        if($r->query("n")){
            $name = $r->query("n");
        }
        $res = Product::all();
        $cat = Category::all();

        if(isset($category) && isset($name)){
            $name = strtolower($name);
            $sRes = DB::select( DB::raw("SELECT * FROM `products` WHERE lower(name) like '%$name%' and category_id = $category" ) );
            
        }
        else if(isset($name)){
            $name = strtolower($name);
            $sRes = DB::select( DB::raw("SELECT * FROM `products` WHERE lower(name) like '%$name%'" ) );
          
        }
        else if(isset($category)){
            $catA = (array)$category;
            $sRes = DB::table('products')
            ->where("category_id", $category)
            //->whereIn('category_id', $catA->get();
            ->get();
          
        }
        else{
            $sRes = DB::table('products')
            ->get();
        }

        if(!isset($category)) {
            $category = -1;
            $title = 'All Products';
        }
        if (session::has('user')) {
            $fav = Favourite::select('product_id')->where('user_id', session('user')->id)->get();
        }
        return view('store.search')
        ->with('fav', $fav)
        ->with('products', $sRes)
        ->with("cat", $cat)
        ->with("a", $category)
        ->with('title', $title);
    }

    public function post($id,orderRequest $r)
    {
        //dd($r->discount_price_holder);

        //dd($r->questions , $r->questionA);
        if(!(Session::has('cart')))
        {
           Session::put('orderCounter',1);
           if ($r->questions ) {


            foreach ($r->questions as $key => $q) {
            //dd($r->questionA[$key]);

                $questions=$id.":".$q.":".$r->questionA[$key].":".Session::get('orderCounter');
                if(!(Session::has('questions')) )
                {
                    Session::put('questions',$questions);
                }
                else{
                  $total_questions_row=Session::get('questions').",".$questions;
                  Session::put('questions',$total_questions_row);
              }

          }
      }


            $c=$id.":".$r->quantity.":".$r->color.":".$r->sizeI.":".$r->priceI.":".Session::get('orderCounter');   //the order counter is added after color so that order serial can be obtained
            Session::put('cart',$c);
        }
        else
        {
         Session::put('orderCounter',Session::get('orderCounter')+1);
         if ($r->questions ) {
            foreach ($r->questions as $key => $q) {
            //dd($r->questionA[$key]);

               $new_questions_row=$id.":".$q.":".$r->questionA[$key].":".Session::get('orderCounter');
               $total_questions_row=Session::get('questions').",".$new_questions_row;
               Session::put('questions',$total_questions_row);
           }
       }


       $cd=$id.":".$r->quantity.":".$r->color.":".$r->sizeI.":".$r->priceI.":".Session::get('orderCounter');
       $total=Session::get('cart').",".$cd;
       Session::put('cart',$total);
   }
       // dd(Session::get('questions'),Session::get('cart'));
   return back()->with('status', 'Product added into cart!');
       // return redirect()->route('user.home');
}

public function cart(Request $r)
    {   //Session::forget('cart');
    $title = 'Cart';
    $res = Product::all();
    $cat = Category::all();
    $shipping = Shipping::all();
    if(!Session::has('cart'))
    {
        return view('store.cart')->with('all',null)
        ->with('title', $title)
        ->with('products',[])
        ->with('products', $res)
        ->with("cat", $cat);
    }
        //dd(Session::get('cart'));
    $cart=[];
    $product=[];
    $ques = [];
    $cost=0;
    $cost_after_quantity=0;
    $totalCart = explode(',',Session::get('cart'));
        //dd($totalCart);
    if(Session::has('questions')){
        $totalQues = explode(',',Session::get('questions'));    
    }

    foreach($totalCart as $c)
    {
        $cart[]=explode(':',$c);
        $a=explode(':',$c);
        $res = Product::find($a[0]);
        $res['size'] = $a[3];
        $res['price'] = $a[4];
        $res['order'] = $a[5];

        if(Session::has('questions')){
            foreach ($totalQues as $que) {
                $an=explode(':',$que);
                if(isset($a[5]) && isset($an[3])){
                    if ($a[5] == $an[3]) {
                       $res['questions'] =  $an;
                   }
               }
           }
       }


       $product[]=$res;
       $cost_after_quantity=$a[1]*$a[4];
       $cost+= $cost_after_quantity;
       Session::put('price',$cost);

   }
   return view('store.cart')
   ->with('products', $res)
   ->with("cat", $cat)
   ->with('all',$cart)
   ->with('title', $title)
   ->with('prod',$product)
   ->with('shipping',$shipping)
   ->with('total',Session::get('price'));
}


//    for quntity control in cart
public function editCart(Request $r)
{    
    $newres = Product::all();
    $newcat = Category::all();
    $newcart=[];
    $newproduct=[];
    $newcost=0;
    $newtotalCart = explode(',',Session::get('cart'));
        //dd(Session::get('cart'));
    foreach($newtotalCart as $c)
    {
        $newcart[]=explode(':',$c);
    }
    foreach($newcart as $t)
    {
        if($t[0]==$r->pid && $t[5]==$r->oSerial)
        {

            $t[1]=$r->newQ;
        }
        if(!(Session::has('tempCart')))
        {

            $str=$t[0].":".$t[1].":".$t[2].":".$t[3].":".$t[4].":".$t[5];
            Session::put('tempCart',$str);
        }
        else
        {
            $str2=$t[0].":".$t[1].":".$t[2].":".$t[3].":".$t[4].":".$t[5];
            $mytotal=Session::get('tempCart').",".$str2;
            Session::put('tempCart',$mytotal);
        }

    }
    Session::forget('cart');
    Session::put('cart',Session::get('tempCart'));
    Session::forget('tempCart');

            //for price update
    $res = Product::all();
    $cat = Category::all();
    $cart=[];
    $product=[];
    $cost=0;
    $cost_after_quantity=0;
    Session::forget('price');
    $totalCart = explode(',',Session::get('cart'));
            //dd(Session::get('cart'));
    foreach($totalCart as $c)
    {
        $cart[]=explode(':',$c);
        $a=explode(':',$c);
        $res = Product::find($a[0]);
        $product[]=$res;
        $cost_after_quantity=$a[1]*$a[4];
        $cost+= $cost_after_quantity;
        Session::put('price',$cost);

    }
            //dd(Session::get('price'));
            //end 
            //dd($myarr);
    $szn[0]=Session::get('cart');
    $szn[1]=Session::get('price');
    $szn[2]=$cost;


    return json_encode($szn);
    exit;    


}
//    for quntity control in cart ENDS

public function deleteCartItem(Request $r)
{


    $counter=0;
    $newtotalCart = explode(',',Session::get('cart'));
    foreach($newtotalCart as $c)
    {
        $newcart[]=explode(':',$c);
    }
    foreach($newcart as $t)
    {   
        if($t[5]==$r->serial)
        {
            $another_counter=$counter;
        }
        $counter++;
    } 
    array_splice($newtotalCart, $another_counter, 1);
    foreach($newtotalCart as $c2)
    {

        $newcart2[]=explode(':',$c2);
    }

    if($newtotalCart==null)
    {
        Session::forget('cart');
        Session::forget('questions');
        Session::forget('price');
        Session::forget('orderCounter');
        return json_encode("Empty");
        exit;     

    }

    else
    {
        foreach($newcart2 as $t2)
        {

            if(!(Session::has('tempCart')))
            {

                $str2=$t2[0].":".$t2[1].":".$t2[2].":".$t2[3].":".$t2[4].":".$t2[5];
                Session::put('tempCart',$str2);


            }
            else
            {
                $str2=$t2[0].":".$t2[1].":".$t2[2].":".$t2[3].":".$t2[4].":".$t2[5];
                $mytotal2=Session::get('tempCart').",".$str2;
                Session::put('tempCart',$mytotal2);
            }

        }

        Session::forget('cart');
        Session::put('cart',Session::get('tempCart'));
        Session::forget('tempCart');

            //for price update
        $res = Product::all();
        $cat = Category::all();
        $cart=[];
        $product=[];
        $cost=0;
        $cost_after_quantity=0;
        Session::forget('price');
        $totalCart = explode(',',Session::get('cart'));
            //dd(Session::get('cart'));
        foreach($totalCart as $c)
        {
            $cart[]=explode(':',$c);
            $a=explode(':',$c);
            $res = Product::find($a[0]);
            $product[]=$res;
            $cost_after_quantity=$a[1]*$a[4];
            $cost+= $cost_after_quantity;
            Session::put('price',$cost);

        }
        $szn[0]=Session::get('cart');
        $szn[1]=Session::get('price');
        $szn[2]=$cost;
        $szn[3]=$r->serial;
        return json_encode($szn);
        exit; 
    }





        //testing ends
}

public function confirm(Request $r)
{  
    if($r->has('order'))
    {
        if(Session::has('user'))
        {

            $sales= new sale();
            $sales->user_id=session('user')->id;
            $sales->product_id=session('cart');
            $sales->order_status='Placed';
            $sales->price=session('price');
            //$sales->notes=$r->notes;
               //Saving Invoice
            $cart=[];
            $product=[];
            $ques = [];
            $totalQues = [];
            $cost=0;
            $cost_after_quantity=0;
            $totalCart = explode(',',Session::get('cart'));
            if(Session::has('questions')){
                $totalQues = explode(',',Session::get('questions'));    
            }

            foreach($totalCart as $c)
            {
                $cart[]=explode(':',$c);
                $a=explode(':',$c);
                $res = Product::find($a[0]);
                $res['qty'] = $a[1];
                $res['order'] = $a[5];
                if(Session::has('questions')){
                    foreach ($totalQues as $que) {
                        $an=explode(':',$que);
                        if(isset($a[5]) && isset($an[3])){
                            if ($a[5] == $an[3]) {
                               $res['questions'] =  $an;
                           }
                       }
                   }
               }
               $res['price'] = $a[4];
               $res['size'] = $a[3];
               $product[]=$res;
               $cost_after_quantity=$a[1]*$a[4];

               $cost+= $cost_after_quantity;
               Session::put('price',$cost);

           }
           $address = Address::find(session('user')->address_id);
           $total_cost =$cost+$r->shipping_price;
           $disc = 0;
           $promo_id = '';
           if($r->promo != ''){
                 $promo = Promo::where('title',$r->promo)
                ->where('status', 1)
                ->first();
                if($promo)
                {
                    $limit = CustomerPromo::where('customer_id',session('user')->id)
                    ->where('promo_id', $promo->id)
                    ->count();

                    if($limit > $promo->limit)
                    {
                        return back()->with('warning', 'Limit to use "'. $promo->title .'" is over!');
                    }

                    if ($promo->type_id == 1) {
                     
                        $promo_id = $promo->id;
                        $discount = $promo->value;
                        $disc = ($discount/100)*$total_cost;
                        $total_cost = $total_cost - $disc;   
                    }
                    else if ($promo->type_id == 2){
                        if ($r->shipping == 3 || $r->shipping == 4) {
                            $promo_id = $promo->id;
                            $discount = $r->shipping_price;
                            $disc = $r->shipping_price;
                            $total_cost = $total_cost - $disc;
                            if($cost < 20.00){
                                return back()->with('warning', 'To Use "'. $promo->title .'", The minimum Subtotal must be at least £20!');
                            }
                        }
                         
                    }
                    else if ($promo->type_id == 3){
                        $promo_id = $promo->id;
                        $discount = $promo->value;
                        $disc = ($discount);
                        $total_cost = $total_cost - $disc;      
                    }
                    else if ($promo->type_id == 4){
                        if ($r->shipping == 1) {
                            $promo_id = $promo->id;
                            $discount = $r->shipping_price;
                            $disc = $r->shipping_price;
                            $total_cost = $total_cost - $disc;
                            if($cost < 20.00){
                                return back()->with('warning', 'To Use this Promo, The minimum Subtotal must be at least £20!');
                            }
                        }
                         
                    }
                }
           }
           /*Dedicated Time*/
           if ($r->shipping == 2) {
                $dt = $r->dedicated_time_i;
            }
            else{
                $dt = "-";
            }
            /* /Dedicated Time */
            
            if($cost < 10.00){
            return back()->with('warning', 'The minimum Subtotal must be at least £10!');
            }

             /* /Shipping Address*/
            if($r->shipping_postcode != '' && $r->shipping_address != '')
            {
                $shipping_address = $r->shipping_address;
                $shipping_postcode = $r->shipping_postcode;
                $shipping_city = $r->shipping_city;
            }
            else
            {
                $shipping_address = $address->area;
                $shipping_postcode = $address->postcode;
                $shipping_city = $address->city;
            }

        /* /Shipping Address*/
        $invoice = Invoice::create(['user_id' => session('user')->id, 'price' => $cost, 'shipping_price' => $r->shipping_price ,'total_price'=> $total_cost, 'postcode' =>  $shipping_postcode, 'Place' =>  $shipping_address, 'city' => $shipping_city, 'paid' => 0,  'shiping_id' => $r->shipping, 'shipping_time' => $dt, 'notes' => $r->notes, 'discount' => $disc]);

        $new_order = array('invoice_no' => $address->postcode,
            'postcode' => $invoice->id);
        
        foreach ($product as $prod) {

            $invoice_detail = InvoiceDetail::create(['invoice_id' => $invoice->id, 'product_id' => $prod->id, 'price' => $prod->price, 'qty' => $prod->qty, 'size'=>$prod->size, 'total_price'=> (($prod->qty)*($prod->price))]);

            foreach ($totalQues as $que) {
                $an=explode(':',$que);

                if(isset($prod->order) && isset($an[3])){
                    if ($prod->order == $an[3]) {
                        $iq = InvoiceQuestion::create(['invoice_detail_id' => $invoice_detail->id, 'question'=>$an[1], 'option' => $an[2] ]);
                    }
                }
            }      
        }

        if($disc > 0)
        {
            CustomerPromo::create(['invoice_id' => $invoice->id, 'customer_id' => session('user')->id, 'promo_id' => $promo_id ]);
        }
        
        $sales->save();
      
        //Now clearing sessions after payments
        // Session::forget('cart');
        // Session::forget('price');
        // Session::forget('orderCounter');
        // Session::forget('questions');
        Session::put('invoice_id',$invoice->id);
        return redirect()->route('checkout',$invoice->id);

    }
    else{
       return redirect()->route('user.login')->with('warning', 'Please log in or create an account to place an order!');
    }

}

if($r->has('signup'))
{ 
    $validatedData = $r->validate([
        'name' => 'required',
        'email' => 'required|email|unique:users',
        'address' => 'required',
        'city' => 'required',
        'zip' => 'required|numeric',
        'tel' => 'required|numeric',
        'pass' => 'required|min:5'
    ]);
            //dd($validatedData);
    $u=new User();
    $add=new Address();
    $add->area=$r->address;
    $add->city=$r->city;
    $add->zip=$r->zip;
    $add->save();
    $add_id=$add->id;
    $u->full_name=$r->name;
    $u->email=$r->email;
    $u->password=$r->pass;
    $u->address_id=$add_id;
    $u->phone=$r->tel;
            //dd($u);
    $u->save();
    $user=User::find($u->id);
    Session::put('user',$user);
    return redirect()->route('user.cart');
}

}
public function history(Request $r)
{
    $title = 'My Orders';
    $res1= Invoice::where('user_id', session('user')->id)->get();
    if(!$res1)
    {
        return view('user.orderHistory')->with('all',[])
        ->with('products',[])
        ->with('sale',[]);
    }

    $cart=[];
    $product=[];
    $id=[];
       
    $cat = Category::all();
       
    return view('store.history')
    ->with("cat", $cat)
    ->with('title', $title)
    ->with('invoice',$res1);
}
public function subscribeNow(Request $request)
{
    $email = $request->email;
    $subscriber = Subscriber::create(['email' => $email, 'status' => 1]);

    return back()->with('status', 'Thanks for subscribing to our newsletter!');;
}

public function add_favourite($prod_id)
{
    if(Session::has('user')) {
       $add = new Favourite();
        $add->user_id = session('user')->id;
        $add->product_id = $prod_id;
        $add->save();
   }
}  
public function aboutUs()
{
    $cat = Category::all();
    $title = 'About US';
    return view('store.aboutUs')->with("cat", $cat)->with('title', $title);
}
public function deliveryProcess()
{
    $cat = Category::all();
    $title = 'Delivery process';
    return view('store.deliveryProcess')->with("cat", $cat)->with('title', $title);
}
public function privacyPolicy()
{
    $cat = Category::all();
    $title = 'Privacy Policy';
    return view('store.privacyPolicy')->with("cat", $cat)->with('title', $title);
}
public function refundPolicy()
{
    $cat = Category::all();
    $title = 'Refund Policy';
    return view('store.refundPolicy')->with("cat", $cat)->with('title', $title);
}
public function termsConditions()
{
    $cat = Category::all();
    $title = 'Terms & Conditions';
    return view('store.t&c')->with("cat", $cat)->with('title', $title);
}

public function siteMap()
{
    $cat = Category::all();
    $title = 'Site Map';
    return view('store.siteMap')->with("cat", $cat)->with('title', $title);
}

}
