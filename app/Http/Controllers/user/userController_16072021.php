<?php
namespace App\Http\Controllers\user;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\orderRequest;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\Category;
use App\sale;
use App\User;
use App\Address;
use Session;
use App\Shipping;
use App\Invoice;
use App\InvoiceDetail;
use App\InvoiceQuestion;
use App\Events\NewOrder;
use App\Http\Controllers\InvoiceController;
use App\Variation;


class userController extends Controller
{
    public function index()
    {
        $res = Product::paginate(8);
        $cat = Category::all();
        return view('store.index')
        ->with('products', $res)
        ->with("cat", $cat)
        ->with('index', 1);
    }
    public function view($id)
    {

        $res = Product::find($id);
        $res1 = Product::all();
        $cat=Category::find($res->category_id);
        $colorList = explode(',',$res->colors);
        $cat = Category::all();
        $var = Variation::where('product_id',$id)->get();
       // dd($var);
        return view('store.product')
        ->with('product', $res)
        ->with('products', $res1)
        ->with('cat', $cat)
        ->with('var', $var)
        ->with('colors',$colorList);   
    }

    public function search(Request $r){
        $category ;
        $name ;
        if($r->query("c")){
            $category = $r->query("c");
        }
        if($r->query("n")){
            $name = $r->query("n");
        }
        $res = Product::all();
        $cat = Category::all();

        if(isset($category) && isset($name)){
            $name = strtolower($name);
            $sRes = DB::select( DB::raw("SELECT * FROM `products` WHERE lower(name) like '%$name%' and category_id = $category" ) );
            //dd("SELECT * FROM `products` WHERE lower(name) like '%$name%' and category_id = $category" );
            //$a = 0;
        }
        else if(isset($name)){
            $name = strtolower($name);
            $sRes = DB::select( DB::raw("SELECT * FROM `products` WHERE lower(name) like '%$name%'" ) );
          //dd("SELECT * FROM `products` WHERE lower(name) like '%$name%'" );
           // $a = 1;
        }
        else if(isset($category)){
            $sRes = DB::table('products')
            ->where("category_id" , $category)
            ->get();
            //$a = 2;
        }
        else{
            $sRes = DB::table('products')
            ->get();
           // $a= 3;
        }

        if(!isset($category)) {
            $category = -1;
        }
        //dd($sRes);
        return view('store.search')
        ->with('products', $sRes)
        ->with("cat", $cat)
        ->with("a", $category);
    }

    public function post($id,orderRequest $r)
    {
        //dd($r->discount_price_holder);

        //dd($r->questions , $r->questionA);
        if(!(Session::has('cart')))
        {
           Session::put('orderCounter',1);
           if ($r->questions ) {


            foreach ($r->questions as $key => $q) {
            //dd($r->questionA[$key]);

                $questions=$id.":".$q.":".$r->questionA[$key].":".Session::get('orderCounter');
                if(!(Session::has('questions')) )
                {
                    Session::put('questions',$questions);
                }
                else{
                  $total_questions_row=Session::get('questions').",".$questions;
                  Session::put('questions',$total_questions_row);
              }

          }
      }


            $c=$id.":".$r->quantity.":".$r->color.":".$r->sizeI.":".$r->priceI.":".Session::get('orderCounter');   //the order counter is added after color so that order serial can be obtained
            Session::put('cart',$c);
        }
        else
        {
         Session::put('orderCounter',Session::get('orderCounter')+1);
         if ($r->questions ) {
            foreach ($r->questions as $key => $q) {
            //dd($r->questionA[$key]);

               $new_questions_row=$id.":".$q.":".$r->questionA[$key].":".Session::get('orderCounter');
               $total_questions_row=Session::get('questions').",".$new_questions_row;
               Session::put('questions',$total_questions_row);
           }
       }


       $cd=$id.":".$r->quantity.":".$r->color.":".$r->sizeI.":".$r->priceI.":".Session::get('orderCounter');
       $total=Session::get('cart').",".$cd;
       Session::put('cart',$total);
   }
       // dd(Session::get('questions'),Session::get('cart'));
   return back()->with('status', 'Product added into cart!');
       // return redirect()->route('user.home');
}

public function cart(Request $r)
    {   //Session::forget('cart');
    $res = Product::all();
    $cat = Category::all();
    $shipping = Shipping::all();
    if(!Session::has('cart'))
    {
        return view('store.cart')->with('all',null)
        ->with('products',[])
        ->with('products', $res)
        ->with("cat", $cat);
    }
        //dd(Session::get('cart'));
    $cart=[];
    $product=[];
    $ques = [];
    $cost=0;
    $cost_after_quantity=0;
    $totalCart = explode(',',Session::get('cart'));
        //dd($totalCart);
    if(Session::has('questions')){
        $totalQues = explode(',',Session::get('questions'));    
    }

    foreach($totalCart as $c)
    {
        $cart[]=explode(':',$c);
        $a=explode(':',$c);
        $res = Product::find($a[0]);
        $res['size'] = $a[3];
        $res['price'] = $a[4];
        $res['order'] = $a[5];

        if(Session::has('questions')){
            foreach ($totalQues as $que) {
                //$ans[] = explode(':', $que);
                $an=explode(':',$que);
                 //$ans[] = explode(':', $an);
                //  echo '<pre>';
                // print_r($an);
                // print_r("Prod");
                // print_r($o);
                if(isset($a[5]) && isset($an[3])){
                    if ($a[5] == $an[3]) {
                       $res['questions'] =  $an;
                   }
               }
           }
       }


       $product[]=$res;
       $cost_after_quantity=$a[1]*$a[4];
       $cost+= $cost_after_quantity;
       Session::put('price',$cost);

   }
        // dd('die');
         //dd($product);
        //dd(Session::get('questions'));

   return view('store.cart')
   ->with('products', $res)
   ->with("cat", $cat)
   ->with('all',$cart)
   ->with('prod',$product)
   ->with('shipping',$shipping)
   ->with('total',Session::get('price'));
}


//    for quntity control in cart
public function editCart(Request $r)
{    
    $newres = Product::all();
    $newcat = Category::all();
    $newcart=[];
    $newproduct=[];
    $newcost=0;
    $newtotalCart = explode(',',Session::get('cart'));
        //dd(Session::get('cart'));
    foreach($newtotalCart as $c)
    {
        $newcart[]=explode(':',$c);
    }
    foreach($newcart as $t)
    {
        if($t[0]==$r->pid && $t[5]==$r->oSerial)
        {

            $t[1]=$r->newQ;
        }
        if(!(Session::has('tempCart')))
        {

            $str=$t[0].":".$t[1].":".$t[2].":".$t[3].":".$t[4].":".$t[5];
            Session::put('tempCart',$str);
        }
        else
        {
            $str2=$t[0].":".$t[1].":".$t[2].":".$t[3].":".$t[4].":".$t[5];
            $mytotal=Session::get('tempCart').",".$str2;
            Session::put('tempCart',$mytotal);
        }

    }
    Session::forget('cart');
    Session::put('cart',Session::get('tempCart'));
    Session::forget('tempCart');

            //for price update
    $res = Product::all();
    $cat = Category::all();
    $cart=[];
    $product=[];
    $cost=0;
    $cost_after_quantity=0;
    Session::forget('price');
    $totalCart = explode(',',Session::get('cart'));
            //dd(Session::get('cart'));
    foreach($totalCart as $c)
    {
        $cart[]=explode(':',$c);
        $a=explode(':',$c);
        $res = Product::find($a[0]);
        $product[]=$res;
        $cost_after_quantity=$a[1]*$a[4];
        $cost+= $cost_after_quantity;
        Session::put('price',$cost);

    }
            //dd(Session::get('price'));
            //end 
            //dd($myarr);
    $szn[0]=Session::get('cart');
    $szn[1]=Session::get('price');
    $szn[2]=$cost;


    return json_encode($szn);
    exit;    


}
//    for quntity control in cart ENDS

public function deleteCartItem(Request $r)
{


    $counter=0;
    $newtotalCart = explode(',',Session::get('cart'));
        //dd(Session::get('cart'));
    foreach($newtotalCart as $c)
    {
        $newcart[]=explode(':',$c);
    }
    foreach($newcart as $t)
    {   
        if($t[5]==$r->serial)
        {
            $another_counter=$counter;
        }
        $counter++;
    } 
    array_splice($newtotalCart, $another_counter, 1);

        //testing Starts
        //dd(Session::get('tempCart'));
    foreach($newtotalCart as $c2)
    {

        $newcart2[]=explode(':',$c2);
    }

    if($newtotalCart==null)
    {
        Session::forget('cart');
        Session::forget('questions');
        Session::forget('price');
        Session::forget('orderCounter');
        return json_encode("Empty");
        exit;     

    }

    else
    {
        foreach($newcart2 as $t2)
        {

            if(!(Session::has('tempCart')))
            {

                $str2=$t2[0].":".$t2[1].":".$t2[2].":".$t2[3].":".$t2[4].":".$t2[5];
                Session::put('tempCart',$str2);


            }
            else
            {
                $str2=$t2[0].":".$t2[1].":".$t2[2].":".$t2[3].":".$t2[4].":".$t2[5];
                $mytotal2=Session::get('tempCart').",".$str2;
                Session::put('tempCart',$mytotal2);
            }

        }

        Session::forget('cart');
        Session::put('cart',Session::get('tempCart'));
        Session::forget('tempCart');

            //for price update
        $res = Product::all();
        $cat = Category::all();
        $cart=[];
        $product=[];
        $cost=0;
        $cost_after_quantity=0;
        Session::forget('price');
        $totalCart = explode(',',Session::get('cart'));
            //dd(Session::get('cart'));
        foreach($totalCart as $c)
        {
            $cart[]=explode(':',$c);
            $a=explode(':',$c);
            $res = Product::find($a[0]);
            $product[]=$res;
            $cost_after_quantity=$a[1]*$a[4];
            $cost+= $cost_after_quantity;
            Session::put('price',$cost);

        }
        $szn[0]=Session::get('cart');
        $szn[1]=Session::get('price');
        $szn[2]=$cost;
        $szn[3]=$r->serial;
        return json_encode($szn);
        exit; 
    }





        //testing ends
}

public function confirm(Request $r)
{  
    if($r->has('order'))
    {
        if(Session::has('user'))
        {

            $sales= new sale();
            $sales->user_id=session('user')->id;
            $sales->product_id=session('cart');
            $sales->order_status='Placed';
            $sales->price=session('price');
            //$sales->notes=$r->notes;
               //Saving Invoice
            $cart=[];
            $product=[];
            $ques = [];
            $totalQues = [];
            $cost=0;
            $cost_after_quantity=0;
            $totalCart = explode(',',Session::get('cart'));
            if(Session::has('questions')){
                $totalQues = explode(',',Session::get('questions'));    
            }

            foreach($totalCart as $c)
            {
                $cart[]=explode(':',$c);
                $a=explode(':',$c);
                $res = Product::find($a[0]);
                $res['qty'] = $a[1];
                $res['order'] = $a[5];
                if(Session::has('questions')){
                    foreach ($totalQues as $que) {
                //$ans[] = explode(':', $que);
                        $an=explode(':',$que);
                 //$ans[] = explode(':', $an);
                // echo '<pre>';
                // print_r($ans);
                        if(isset($a[5]) && isset($an[3])){
                            if ($a[5] == $an[3]) {
                               $res['questions'] =  $an;
                           }
                       }
                   }
               }
           //dd('die');
               $res['price'] = $a[4];
               $res['size'] = $a[3];
               $product[]=$res;
               $cost_after_quantity=$a[1]*$a[4];

               $cost+= $cost_after_quantity;
               Session::put('price',$cost);

           }
           $address = Address::find(session('user')->id);
           $total_cost =$cost+$r->shipping_price;
           if ($r->shipping == 2) {
            $dt = $r->dedicated_time_i;
        }
        else{
            $dt = "-";
        }
        $invoice = Invoice::create(['user_id' => session('user')->id, 'price' => $cost, 'shipping_price' => $r->shipping_price ,'total_price'=> $total_cost, 'postcode' => $address->postcode, 'Place' => $address->area, 'paid' => 0,  'shiping_id' => $r->shipping, 'shipping_time' => $dt, 'notes' => $r->notes]);

        $new_order = array('invoice_no' => $address->postcode,
            'postcode' => $invoice->id);
        broadcast(new NewOrder($new_order));
        
        foreach ($product as $prod) {

            $invoice_detail = InvoiceDetail::create(['invoice_id' => $invoice->id, 'product_id' => $prod->id, 'price' => $prod->price, 'qty' => $prod->qty, 'size'=>$prod->size, 'total_price'=> (($prod->qty)*($prod->price))]);

            foreach ($totalQues as $que) {
                $an=explode(':',$que);

                if(isset($prod->order) && isset($an[3])){
                    if ($prod->order == $an[3]) {
                        $iq = InvoiceQuestion::create(['invoice_detail_id' => $invoice_detail->id, 'question'=>$an[1], 'option' => $an[2] ]);
                    }
                }
            }      
        }
        // $email = session('user')->email;

        
        // $mail = (new InvoiceController)->mail($invoice->id,$email);   
        // $admin_mail = "thehalalbutcheryuk@gmail.com";
        // $mail1 = (new InvoiceController)->mail($invoice->id,$admin_mail);
        $sales->save();
       //      if (session('user')->email == "usman@goldenarts.co.uk") {
       //  return redirect()->route('checkout',$invoice->id);              
       // }

        Session::forget('cart');
        Session::forget('price');
        Session::forget('orderCounter');
        Session::forget('questions');
        Session::put('invoice_id',$invoice->id);
        return redirect()->route('checkout',$invoice->id);

    }
    else{
        return redirect()->route('user.cart');
    }

}

if($r->has('signup'))
{ 
    $validatedData = $r->validate([
        'name' => 'required',
        'email' => 'required|email|unique:users',
        'address' => 'required',
        'city' => 'required',
        'zip' => 'required|numeric',
        'tel' => 'required|numeric',
        'pass' => 'required|min:5'
    ]);
            //dd($validatedData);
    $u=new User();
    $add=new Address();
    $add->area=$r->address;
    $add->city=$r->city;
    $add->zip=$r->zip;
    $add->save();
    $add_id=$add->id;
    $u->full_name=$r->name;
    $u->email=$r->email;
    $u->password=$r->pass;
    $u->address_id=$add_id;
    $u->phone=$r->tel;
            //dd($u);
    $u->save();
    $user=User::find($u->id);
    Session::put('user',$user);
    return redirect()->route('user.cart');
}

}
public function history(Request $r)
{
    $res1= Invoice::where('user_id', session('user')->id)->get();
    if(!$res1)
    {
        return view('user.orderHistory')->with('all',[])
        ->with('products',[])
        ->with('sale',[]);
    }

    $cart=[];
    $product=[];
    $id=[];
       
    $cat = Category::all();
       
    return view('store.history')
    ->with("cat", $cat)
    ->with('invoice',$res1);
}

public function aboutUs()
{
    $cat = Category::all();
    return view('store.aboutUs')->with("cat", $cat);
}
public function deliveryProcess()
{
    $cat = Category::all();
    return view('store.deliveryProcess')->with("cat", $cat);
}
public function privacyPolicy()
{
    $cat = Category::all();
    return view('store.privacyPolicy')->with("cat", $cat);
}
public function refundPolicy()
{
    $cat = Category::all();
    return view('store.refundPolicy')->with("cat", $cat);
}
public function termsConditions()
{
    $cat = Category::all();
    return view('store.t&c')->with("cat", $cat);
}
}
