<?php

namespace App\Http\Controllers\MobileApi\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Admin;
use App\Invoice;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function dashboard($page)
    {
        $model = Invoice::with('user', 'shipping')->orderBy('id', 'DESC')->get();
        //dd($model);
        if ($page == "Dashboard") {
            $model = Invoice::with('user', 'shipping')->orderBy('id', 'DESC')->get();
        }
        else if ($page == "New") {
            $model = Invoice::with('user', 'shipping')->where('status',"Placed")->orderBy('id', 'DESC')->get();
        }
         else if ($page == "Processing") {
            $model = Invoice::with('user', 'shipping')->where('status',"Processing")->orderBy('id', 'DESC')->get();
        }
         else if ($page == "Dispatched") {
            $model = Invoice::with('user', 'shipping')->where('status',"Dispatched")->orderBy('id', 'DESC')->get();
        }
        else if ($page == "Deliverd") {
            $model = Invoice::with('user', 'shipping')->where('status',"Deliverd")->orderBy('id', 'DESC')->get();
        }
        
         $response = ["message"=>"No Order","error"=>false,'order' => $model];
            return json_encode($response);
    
    }

     public function invoiceUpdate(Request $request)
    {
        $id = $request->id;
        $status = $request->status;
        DB::table('invoices')
            ->where('id', $id)
            ->update(['status' => $status]);
        $response = ["message"=>"Order Updated","error"=>false];
          return json_encode($response);
    }

    public function showInvoice(Request $request)
    {
        $id = $request->id;
        $invoice = Invoice::with('user', 'shipping')->find($id);
        $response = ["message"=>"Order Updated","error"=>false, "invoice"=>$invoice];
          return json_encode($response);
    }

}
