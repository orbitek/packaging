<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Postcode;
use App\Admin;
use DB;
use Auth;
use Session;
class PostcodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Postcode::all();

        return view('admin_panel.postcodes.index')
            ->with('postcodelist', $result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//         postcode
// area
// city
         $user = Postcode::create($request->all());
        // $role = Role::where('id',$request['role'])->first();
        // $user->roles()->attach($role);
        // $this->userRoleUpdate($request,$user->id);
        return back()->with('flash_success','Postcode added successfully ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat = Postcode::find($id);
        $butchers = Admin::whereHas("roles", function($q){ $q->where("name", "Butcher"); })->get();
        //$butchers = DB::table('admin_role')->where('role_id',2)->get();
    
        //dd($butchers); 
        return view('admin_panel.postcodes.edit')
            ->with('postcode', $cat)
            ->with('butchers', $butchers);
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $admin = Session::get('admin');
        $request['status'] = 1;
        //dd($request->all());
        $postcode = Postcode::find($id);
        $postcode->update($request->all());

        $butcher = Admin::find($request->butcher);
        $butcher['assign_by'] = $admin->id;
        
        //$role = Role::where('id',$request['role'])->first();
         //$user->roles()->attach($role);

        $admin->postcodes()->sync($butcher);
        return redirect()->route('Postcode.PostcodeManagement')->with('success',"Postcode Updated Succesfully");
        // dd($id);
        // dd($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function maps(){
         return view('Maps.index');
    }
}
