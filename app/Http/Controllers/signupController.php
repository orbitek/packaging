<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Illuminate\Support\Facades\DB;
use App\Product;
use App\Category;
use App\sale;
use App\User;
use App\Address;
use Session;
use Sentinel;
use Reminder;
use Hash;
use App\ForgetPassword;
use App\Http\Controllers\InvoiceController;


class signupController extends Controller
{
    
    public function userIndex()
    {

        
        if(session()->has('user')){
            return redirect()->route("user.cart");
        }

        $res = Product::all();
        $cat = Category::all();
        $title = 'SignUp';
       
        
    	return view('store.signup')
        ->with('title', $title)
        ->with('products', $res)
        ->with("cat", $cat);
    }
    
    public function userPosted(Request $r)
    {
        
        
       // dd($r);
        
            
            $validatedData = $r->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'address' => 'required',
            'city' => 'required',
            'postcode' => 'required',
            'tel' => 'required|numeric',
            'pass' => 'required|min:5',
            'confirm_password' => 'required|min:5|same:pass'
            ]);

            //dd($validatedData);
            $u=new User();
            $add=new Address();
            $add->area=$r->address;
            $add->city=$r->city;
            $add->postcode=$r->postcode;

            $add->save();
            $add_id=$add->id;

            $u->full_name=$r->name;
            $u->email=$r->email;
            $u->password=$r->pass;
            $u->address_id=$add_id;
            $u->phone=$r->tel;
            
            //dd($u);

            $u->save();

            $user=User::find($u->id);

            $r->session()->put('user',$user);

            return redirect()->route('user.home');
        
       
    }
     public function editProfile()
    {
        
         $cat = Category::all();
       $user_id=session('user')->id;
        $user=User::find($user_id);
        $title = 'Profile';
           // dd($user);
        return view('store.editProfile')->with("cat", $cat)->with('user',$user)->with('title', $title);
    }
    public function updateProfile(Request $r)
    {


            $user_id=session('user')->id;
            $u=User::find($user_id);

            $add=Address::find($u->addresses->id);
            $add->area=$r->address;
            $add->city=$r->city;
            $add->postcode=$r->postcode;

            $add->update();
            $add_id=$add->id;

            $u->full_name=$r->name;
            $u->email=$r->email;
            $u->password=$r->pass;
            
            if ($user_id == 11) {
                //$u->password = Hash::make($r->pass);
            }
            $u->address_id=$add_id;
            $u->phone=$r->tel;
            
            $u->update();

            $user=User::find($u->id);

            $r->session()->put('user',$user);

            return redirect()->route('editProfile');
    }
    
    public function emailCheck(Request $r)
    {
       $user = User::where('email',$r->email)->first();

        if($user==null)
        {
             $emailstate = 0;
        }
        else
        {
            $emailstate = 1;
        }
        
         echo json_encode($emailstate);
    exit;
    }

     public function forgetPassword()
    {
        
        $cat = Category::all();
        $title = 'Forget Password';
        return view('store.forgetPassword')->with("cat", $cat)->with('title', $title);
    }

    public function forgetPasswordLinkSend(Request $r)
    {

        $user = User::where('email',$r->email)->first();

        if($user==null)
        {
               return back()->with('error', 'This Email is not Registerd!');
        }
        else
        {
            $link = "https://thehalalbutchery.com/".$user->id."/".$user->email;
             $u=new ForgetPassword();
           

            $u->user_id=$user->id;
            $u->email=$user->email;
            $u->link=$link;
            $u->active=1;
            
            $u->save();
            
            $u->link = "https://thehalalbutchery.com/resetPassword/".$user->id."/".$user->email."/".$u->id;
            $u->update();

            $mail = (new InvoiceController)->forgetPasswordLinkEmail($u->link,$user->email); 
            return back()->with('status', 'Please Check your email to reset the password!');
        }


    }

    public function resetPassword($id, $email, $fid)
    {
        $f = ForgetPassword::find($fid);
        if ($f->active == 1 && $f->email == $email && $f->user_id == $id) {

            $cat = Category::all();
            $title = 'Reset Password';
            return view('store.resetPassword')->with("cat", $cat)->with('title', $title)->with('id',$id)->with('fid',$fid);
        }
        else{
            return redirect()->route('user.home')->with('error', 'Your reset password link is Invalid!');

        }
       
    }

    public function resetPasswordSave(Request $r)
    {  
        $id = $r->id;
        $password = $r->pass;
        $u=User::find($id);
        $u->password=$password;
        $u->update();

        $uf=ForgetPassword::find($r->fid);
        $uf->active=0;
        $uf->update();

        return redirect()->route('user.home')->with('status', 'Your Password reset successfully!');

    }

    public function forgotPassworCodeSent()
    {
        $email = "usman@goldenarts.co.uk";
        $user = User::where('email',$email)->first();

        //dd($user);
        if($user==null)
        {
             $emailstate = 0;
        }
        else
        {
            $user = Sentinel::findById($user->id);
            $reminder = Reminder::exists($user) ? : Reminder::create($user);

             $mail = (new InvoiceController)->forgotMail($user,$reminder->code); 
             return "sent";
        }


    }
    public function smsVerify()
    {
        $basic  = new \Vonage\Client\Credentials\Basic("6e9ce445", "NZC1L6pmOEFnnx8p");
        $client = new \Vonage\Client(new \Vonage\Client\Credentials\Container($basic));

        $request = new \Vonage\Verify\Request(447852614649, "THB");
        $response = $client->verify()->start($request);

        echo "Started verification, `request_id` is " . $response->getRequestId();
        dd($response);

    }

    public function smsVerifyCode()
    {
         $basic  = new \Vonage\Client\Credentials\Basic("6e9ce445", "NZC1L6pmOEFnnx8p");
        $client = new \Vonage\Client(new \Vonage\Client\Credentials\Container($basic));
        $result = $client->verify()->check("d6b1738d6c014e8aadeede4c408125b4", 6538);

        dd($result);
        var_dump($result->getResponseData());

    }
    
}
