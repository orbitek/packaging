<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceQuestion extends Model
{
     protected $fillable = [
        'invoice_detail_id',
		'question',
		'option',
    ];
}
