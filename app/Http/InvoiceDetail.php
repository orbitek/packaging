<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model
{
     protected $fillable = [
      'invoice_id',
		'product_id',
		'price',
		'qty',
		'total_price',
      'size',
    ];

    public function product()
    {
    	return $this->belongsTo('App\Product', 'product_id');
    }
    public function invoice_question()
    {
    	return $this->hasMany('App\InvoiceQuestion', 'invoice_detail_id');
    }
}
